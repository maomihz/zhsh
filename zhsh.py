import html2text
import requests
import re
from time import time, sleep

global_delay = 1.5

class zhsh:
    def __init__(self, start_page, delay=global_delay):
        url_regex = re.compile(r'(https?:\/\/(?:.+\.)+?.+?)(\/.*)')
        match = url_regex.match(start_page)
        self.prefix = match.group(1)
        self.current_page = match.group(2)

        self.selected = None
        self.current_content = None
        self.delay = delay
        self.last_op = 0

    def select(self, content):
        pattern = re.compile(r'\[{}\]\((\/\d+\/.*\/)\)'.format(content))
        if self.current_content:
            m = pattern.search(self.current_content)
            if m:
                firstmatch = m.group(1)
                self.selected = firstmatch
                return True
        return False

    def click(self, content=None):
        result = True
        if content:
            result = self.select(content)
        if not result:
            return False
        self.current_page = self.selected
        self.load()
        return True

    def click_new(self, content=None):
        new_obj = self.new()
        if content:
            new_obj.select(content)
        new_obj.click()
        return new_obj

    def click_all(self, content=None):
        pattern = re.compile(r'\[{}\]\((\/\d+\/.*\/)\)'.format(content))
        if self.current_content:
            m = pattern.findall(self.current_content)
            if m:
                firstmatch = m.group(1)
                self.selected = firstmatch
                return True


    def new(self):
        new_obj = zhsh(self.prefix + self.current_page, self.delay)
        new_obj.current_content = self.current_content
        new_obj.selected = self.selected
        new_obj.current_page = self.current_page
        new_obj.last_op = self.last_op
        return new_obj

    def load(self):
        self.__delay()

        r = requests.get(self.prefix + self.current_page)
        self.current_content = html2text.html2text(r.text)

        if self.contains('您的访问频率过快'):
            sleep(4)
            self.select('返回游戏')
            self.load_selected()
            self.__delay()
            self.load()
        if self.contains('简直是手机中的战斗机'):
            sleep(2)
            self.load()

        self.last_op = time()
        # print(self.current_content)
        print(zhsh.clean(self.current_content))

    def load_selected(self):
        self.__delay()

        requests.get(self.prefix + self.selected)

        self.last_op = time()
        # print(zhsh.clean(self.current_content))

    @staticmethod
    def clean(page):
        link = re.compile(r'\[.+\]\(.+\)', re.MULTILINE)
        s = re.sub(link, '', page)
        s = re.sub(r'^[\w]*\.$[\w]*', '', s)
        return "".join([a for a in s.strip().splitlines(True) if a.strip() and not a.strip() == '.'])

    def __delay(self):
        current_time = time()
        diff = current_time - self.last_op
        if diff < self.delay:
            sleep(self.delay - diff)

    def contains(self, content):
        pattern = re.compile(content)
        return pattern.search(self.current_content)

    def searchtext(self, content):
        pattern = re.compile(content)
        result = pattern.search(self.current_content)
        if result:
            return result.group(1)

    def searchall(self, content):
        pattern = re.compile(content)
        results = pattern.findall(self.current_content)
        return results
