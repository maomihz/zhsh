from zhsh import zhsh
from time import sleep

init_link = input('Input ==> ')
z = zhsh(init_link)
z.load()

while True:
    current_location = z.searchtext('(.{2})(:?★码头|市场)')
    if current_location == '大阪':
        destination = '京都'
    else:
        destination = '大阪'

    print(current_location, destination)
    sleep(5)

    z.click('买货')

    while not z.contains('已经满载了'):
        while z.click('\d+箱'):
            z.click('继续买货')

        sleep(5)
        z.load()

    z.click('返回.+')
    z.click('★码头')
    z.click('出航')
    z.click(destination)
    z.click('(?:补给)?出航\(\d+铜贝\)')

    z.delay = 3
    while not z.contains('你已经进入.+码头'):
        z.load()
    z.delay = 1.5

    z.click('市场')
    z.click('卖货')
    z.click('全部卖出')
    z.click('返回.+')
