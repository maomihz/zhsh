from zhsh import zhsh
import re
import random

init_links = [
    'http://zhsh.ppsea.com/185/p?p_W/3329_594471f2_7b213a8e_7d07e027-ad13fa5a-3001830b/1222/',
    'http://zhsh.ppsea.com/185/p?p_W/3329_5944732c_00fa7000_7d07e027-ad13fa5a-1922c7c3/1223/'
]

init_link = init_links[0]

z = zhsh(init_link)
z.load()

z.delay = 0.2
monster_list = []
refresh_link = None

attack_list = ['养人毒蝎']

while True:
    for i in attack_list:
        if z.click(i):
            break
    else:
        if not z.contains('攻击'):
            try:
                refresh_link.load_selected()
                z.load()
                continue
            except:
                zhsh(init_links[1]).load()
    while not z.contains('战胜了') and not z.contains('恭喜你升到'):
        if not z.click('攻击'):
            break
    if z.contains('已完成！'):
        exit()
    if not refresh_link:
        refresh_link = z.new()
        refresh_link.load()
        refresh_link.select('返回')
    z.click('继续')
