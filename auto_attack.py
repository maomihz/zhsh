from zhsh import zhsh
import re

init_link = input('Input ==> ')
z = zhsh(init_link)
z.load()

z.delay = 0.5
monster_list = []
refresh_link = None

try:
    if not monster_list:
        r = re.compile('你看到:((?:\[.+?\]\(.+?\)\n?)+)', re.MULTILINE)
        monsters = r.search(z.current_content).group(1).split()
        monster_set = set()
        for i in monsters:
            r2 = re.compile('^\[(.+)\]\((.+)\)$')
            monster_set.add(r2.match(i).group(1))
        monster_list.extend(z.searchall('(?:任务进度：)?(.+)\(\d+\/\d+\)！?'))
        monster_set = list(monster_set)
        print(monster_set)
        print(monster_list)

        print('选择要打的怪：')
        print('0 - 刷新页面')
        for i, monster in enumerate(monster_set):
            print('{} - {}'.format(i+1, monster))

        default_choices = []
        for i in monster_list:
            if i in monster_set:
                default_choices.append(str(monster_set.index(i) + 1))
        default_choice = ''.join([a for a in default_choices if str(a).isnumeric()])

        user_choice = list(input('请选择：({}) ==> '.format(''.join(default_choice)))) or default_choice
        attack_list = [monster_set[int(a) - 1] for a in user_choice]
        print(attack_list)
except:
    attack_list = []
while True:
    for i in attack_list:
        if z.click(i):
            break
    else:
        if not z.contains('攻击'):
            refresh_link.load_selected()
            z.load()
            continue
    while not z.contains('战胜了') and not z.contains('恭喜你升到'):
        if not z.click('攻击'):
            break
    if z.contains('已完成！'):
        exit()
    if not refresh_link:
        refresh_link = z.new()
        refresh_link.load()
        refresh_link.select('返回')
    z.click('继续')
